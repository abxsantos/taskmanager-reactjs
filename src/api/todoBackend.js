import axios from "axios";
import { REACT_APP_BACKEND_URL } from "../environment.js";

export default axios.create({
  type: "no-cors",
  baseURL: REACT_APP_BACKEND_URL,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});
