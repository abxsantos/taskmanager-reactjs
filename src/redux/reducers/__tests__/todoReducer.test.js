import reducers from "..";

describe("Todo reducer", () => {
  it("should return the initial state", () => {
    expect(reducers(undefined, {})).toEqual({ todos: [] });
  });
  it("should store the GET request data", () => {
    const payloadData = { id: 1, completed: true, description: "ABC" };
    expect(
      reducers(
        { todos: [] },
        {
          type: "GET_TODOS",
          payload: payloadData,
        }
      )
    ).toEqual({ todos: payloadData });
  });
  it("should store the POST request data", () => {
    const payloadData = { id: 1, completed: true, description: "ABC" };
    expect(
      reducers(
        { todos: [] },
        {
          type: "POST_TODO",
          payload: payloadData,
        }
      )
    ).toEqual({ todos: [payloadData] });
  });
  it("Should remove the deleted data after DELETE request", () => {
    expect(
      reducers(
        { todos: [{ id: 1, description: "ok", completed: false }] },
        { type: "DELETE_TODO", payload: 0 }
      )
    ).toEqual({
      todos: [],
    });
  });
  it("Should update the completed value from state after sending PUT request", () => {
    expect(
      reducers(
        { todos: [{ id: 1, description: "ok", completed: false }] },
        {
          type: "UPDATE_TODO",
          payload: { id: 1, description: "ok", completed: true },
          index: 0,
        }
      )
    ).toEqual({
      todos: [{ id: 1, description: "ok", completed: true }],
    });
  });
});
