const handleDeleteTodo = (todoIndex, state) => {
  const updatedTodos = state.filter((_, index) => index !== todoIndex);
  return updatedTodos;
};
const handleUpdateTodo = (updatedTodo, todoIndex, state) => {
  const updatedTodos = state
  updatedTodos[todoIndex] = updatedTodo
  return updatedTodos
};

export default (state = [], action) => {
  switch (action.type) {
    case "GET_TODOS":
      return action.payload;
    case "POST_TODOS":
      return [...state, action.payload];
    case "DELETE_TODO":
      return [...handleDeleteTodo(action.payload, state)];
    case "UPDATE_TODO":
      return [...handleUpdateTodo(action.payload, action.index, state)];
    default:
      return state;
  }
};
