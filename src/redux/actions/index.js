import todoBackend from "../../api/todoBackend";

export const getTodos = () => async (dispatch) => {
  const response = await todoBackend.get("/todos/");
  if (response.status === 200) {
    dispatch({ type: "GET_TODOS", payload: response.data });
  } else {
    window.alert(response.status);
  }
};

export const postTodo = (task) => async (dispatch) => {
  const addedTask = {
    description: task,
    completed: false,
  };
  const response = await todoBackend.post("/todos/", addedTask);
  if (response.status === 201) {
    dispatch({ type: "POST_TODOS", payload: response.data });
  } else {
    window.alert(response.status);
  }
};

export const deleteTodo = (id, todoIndex) => async (dispatch) => {
  const response = await todoBackend.delete(`/todos/${id}/`);
  if (response.status === 204) {
    dispatch({ type: "DELETE_TODO", payload: todoIndex });
  } else {
    window.alert(response.status);
  }
};

export const updateTodo = (task, id, status, todoIndex) => async (dispatch) => {
  const updatedTask = {
    description: task,
    completed: !status,
  };
  const response = await todoBackend.put(`/todos/${id}/`, updatedTask);
  if (response.status === 200) {
    dispatch({ type: "UPDATE_TODO", payload: response.data, index: todoIndex });
  } else {
    window.alert(response.status);
  }
};
