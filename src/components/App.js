import React from "react";
import Grid from "@material-ui/core/Grid";
import TodoTextField from "./TodoTextField";
import TodoList from "./TodoList";

function App() {
  return (
    <div className="App" >
      <Grid
        container
        justify="flex-start"
        alignItems="center"
        direction="column"
        style={{ paddingTop: "100px" }}
      >
        <h1>Todo list</h1>
        <TodoTextField />
        <TodoList />
      </Grid>
    </div>
  );
}

export default App;
