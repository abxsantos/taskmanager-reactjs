import React, { useEffect } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {
  List,
  ListItemText,
  ListItem,
  Checkbox,
  ListItemSecondaryAction,
  IconButton,
} from "@material-ui/core";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import { getTodos, deleteTodo, updateTodo } from "../redux/actions";
import Spinner from "./Spinner";

function TodoList({ todos, getTodos, deleteTodo, updateTodo }) {
  useEffect(() => {
    getTodos();
  }, [getTodos]);

  if (todos !== undefined) {
    return (
      <List>
        {todos.map((task, keyValue) => (
          <ListItem key={keyValue.toString()}>
            <Checkbox
              tabIndex={keyValue}
              id={task.id.toString()}
              name={task.description}
              checked={task.completed}
              onChange={() => {
                updateTodo(task.description, task.id, task.completed, keyValue);
              }}
            />
            <ListItemText primary={task.description} />
            <ListItemSecondaryAction>
              <IconButton
                aria-label="Delete"
                deletetask={keyValue}
                onClick={() => {
                  deleteTodo(task.id, keyValue);
                }}
              >
                <DeleteForeverIcon />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        ))}
      </List>
    );
  } else {
    return (
      <>
        <Spinner message="Loading..."/>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return { todos: state.todos };
};

TodoList.propTypes = {
  todos: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      description: PropTypes.string.isRequired,
      completed: PropTypes.bool.isRequired,
    })
  ),
};

export default connect(mapStateToProps, { getTodos, deleteTodo, updateTodo })(
  TodoList
);
