import React, { useState } from "react";
import TextField from "@material-ui/core/TextField";
import { connect } from "react-redux";
import { postTodo } from "../redux/actions";

function TodoTextField({ postTodo }) {
  const [task, setTask] = useState("");
  return (
    <form
      onSubmit={(event) => {
        event.preventDefault();
        postTodo(task);
        setTask("");
      }}
    >
      <TextField
        placeholder="Add a task"
        onChange={(event) => {
          setTask(event.target.value);
        }}
        value={task}
      ></TextField>
    </form>
  );
}

export default connect(null, { postTodo })(TodoTextField);
