# Task manager frontend

This project was created using ReactJS as a frontend for the [Task manager backend](https://gitlab.com/abxsantos/taskmanager-django).<br />
The main goal was to build the API and frontend in a single day, following best practices (documenting, testing, clean coding), also learn some CRUD requests for the API and frontend with the usage of functional components in React (utilizing hooks).<br />

This App was also used as a test ground for building a container, which can be found [here](https://hub.docker.com/repository/docker/abxsantos/taskmanager-reactjs)
The app is currently deployed at https://taskmanager-reactjs.herokuapp.com/<br />

# Running the container

First it's needed to have docker configured in your machine, or use [docker playground](https://labs.play-with-docker.com/).<br/>
To run simply use: <br />
`docker run -it --rm -v ${PWD}:/app -v /app/node_modules -p 3001:3000 -e CHOKIDAR_USEPOLLING=true taskmanager-reactjs`

# Running the project

To run the app in development mode you can use<br />
### `npm start`
And open [http://localhost:3000](http://localhost:3000) to view it in the browser.<br />

### `npm run build`
Builds the app for production to the `build` folder.<br />
