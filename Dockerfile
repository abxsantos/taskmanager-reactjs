# build phase
FROM node:alpine as builder
WORKDIR /app
COPY package.json .
RUN npm ci && react-scripts@3.4.1 -g
COPY . .
RUN npm run build

# production phase
FROM nginx:stable-alpine
EXPOSE 4000
COPY --from=builder /app/build /usr/share/nginx/html